ssh_key_path="/c/users/blasiak/.ssh/id_rsa_zoom_github"
# You can use these ANSI escape codes:
 
# Black        0;30     Dark Gray     1;30
# Red          0;31     Light Red     1;31
# Green        0;32     Light Green   1;32
# Brown/Orange 0;33     Yellow        1;33
# Blue         0;34     Light Blue    1;34
# Purple       0;35     Light Purple  1;35
# Cyan         0;36     Light Cyan    1;36
# Light Gray   0;37     White         1;37
# And then use them like this in your script:

RED='\033[0;31m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
LIGHTGREEN='\033[1;32m'
ORANGE='\033[0;33m'
PURPLE='\033[0;35m'
LIGHTGREY='\033[0;37m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m' # No Color

# printf "I ${RED}love${NC} Stack Overflow\n"

while true; do
read -p $'\e[33mDo you want to restart ssh-agent? \e[0m: ' yn
    case $yn in
        [Yy]* ) 
			get_pid=$(ps -ef | grep ssh-agent | awk '{print $2}');
			for pid in $get_pid; 
				do 
					kill -9 $pid;
					#echo -e "${WHITE}killed $pid"; 
				done;
			eval `ssh-agent -s`;
			ssh-add ${ssh_key_path};
			break;;
        [Nn]* )
			break;;
        * ) echo -e "${RED}Please answer yes or no.";;
    esac
done


ALIASES=(
	"alias grep='grep --color'"
	"alias ls='ls --color'"
	"alias ll='ls -ltrha'"
	)

LEN=${#ALIASES[@]}
for (( i=0; i<$LEN; i++ )); 
	do
		echo -e "${LIGHTCYAN}setting up an alias: ${RED}${ALIASES[$i]}"
		eval ${ALIASES[$i]}
	done



export HISTFILESIZE=999999

