#!/bin/python

import os
import time

import paramiko
from scp import SCPClient

from smoothlogging import smoothlogging

server = "192.168.1.25"
pdrives = ['g', 'i', 'j']
drives = []
source = "DCIM"

ssh_key = "c:/Users/seba/.ssh/id_rsa_winhp"
destination_dir = "/home/seba/photos/incoming/"
script_name = os.path.basename(__file__)
log_obj = smoothlogging()
log = log_obj.log("c:/temp/", script_name)
ssh_client = paramiko.SSHClient()

for drive in pdrives:
    log.info("checking for drives presence: %s" % (drive))
    if os.path.exists(os.path.join((drive + ":/"), "DCIM")):
        log.info("found drive: %s" % (drive))
        drives.append(drive)


def scp_copy(file_path, server, destination_dir):
    private_key = paramiko.RSAKey.from_private_key_file(ssh_key)
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(server, 8883, 'seba', pkey=private_key, allow_agent=True, look_for_keys=True, compress=False)
    scp = SCPClient(ssh_client.get_transport())
    scp.put(file_path, remote_path=destination_dir,preserve_times=True)


def list_all_files_in_directory(dir):
    list_all = []
    for root, dirs, files in os.walk(dir):
        for file in files:
            list_all.append(os.path.join(root, file))
    return list_all


for drive in drives:
    source_dir = os.path.join(drive + ":/", source)
    if os.path.exists(source_dir):
        log.info("found: %s" % source_dir)
        for f in list_all_files_in_directory(source_dir):
            try:
                scp_copy(f, server, destination_dir)
                log.info("copied (scp) %s into %s on %s" % (f, destination_dir, server))
                os.unlink(f)
            except Exception as e:
                log.error("could not scp file: %s" % (f))
                log.error("exception content: %s" % (e))
