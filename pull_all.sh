#!/bin/bash
RED='\033[0;31m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
LIGHTGREEN='\033[1;32m'
ORANGE='\033[0;33m'
PURPLE='\033[0;35m'
LIGHTGREY='\033[0;37m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m' # No Color

echo -e "${LIGHTCYAN}* Pull for all repos in current dir and subdirs\n"

for repo in `find . -type d -name .git`;
        do repo=$(dirname $repo)
        cd ${repo} 
        echo -e "${ORANGE}pulling changes for: ${repo}";
        git pull 
	sleep 1
	#git fetch --all
        if [[ $? -eq 0 ]]; then
            echo -e "${GREEN}---OK---${NC}";
        else
            echo -e "${RED}---NOK -> ${repo} - is this a GIT REPO?";
        fi  
        cd -
done
